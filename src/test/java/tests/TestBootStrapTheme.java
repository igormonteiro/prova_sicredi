package tests;

import config.BaseTest;
import org.junit.Test;
import pages.PageBootStrapTheme;

public class TestBootStrapTheme extends BaseTest {

    //TODO: DESAFIO 01
    @Test
    public void deveClicarEmOpcaoComboPreencherCamposSalvarEValidar(){
        new PageBootStrapTheme(driver).
                clicarAguardarPaginaPreenchimento();

        new PageBootStrapTheme(driver).
                preencherCampos().
                clickBotaoSaveEValidarMensagem();
    }

    //TODO: DESAFIO 02
    @Test
    public void deveRealizarCadostroEDeletarRegistro(){
        new PageBootStrapTheme(driver).
                clicarAguardarPaginaPreenchimento();

        new PageBootStrapTheme(driver).
                preencherCampos().
                clickBotaoSaveEValidarMensagem().
                pesquisarRegistro();

        new PageBootStrapTheme(driver).
                deletarRegistro();

    }


}
