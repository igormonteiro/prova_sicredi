package pages;

import config.BasePage;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBootStrapTheme extends BasePage {

    public PageBootStrapTheme(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "switch-version-select")
    public WebElement combo_versao;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[1]/div[2]/form/div[1]/div[1]/a")
    public WebElement button_addCustomer;

    @FindBy(id = "field-customerName")
    public WebElement field_name;

    @FindBy(id = "field-contactLastName")
    public WebElement field_lastName;

    @FindBy(id = "field-contactFirstName")
    public WebElement field_contactFirstName;

    @FindBy(id = "field-phone")
    public WebElement field_phone;

    @FindBy(id = "field-addressLine1")
    public WebElement field_addressLine1;

    @FindBy(id = "field-addressLine2")
    public WebElement field_addressLine2;

    @FindBy(id = "field-city")
    public WebElement field_city;

    @FindBy(id = "field-state")
    public WebElement field_state;

    @FindBy(id = "field-postalCode")
    public WebElement field_postalCode;

    @FindBy(id = "field-country")
    public WebElement field_country;

    @FindBy(id = "field_salesRepEmployeeNumber_chosen")
    public WebElement field_selectFromEmployer;

    @FindBy(id = "field-creditLimit")
    public WebElement field_creditLimit;

    @FindBy(id = "form-button-save")
    public WebElement button_save;

    @FindBy(xpath = "/html/body/div[2]/div/div/div/div[2]/form/div[14]/div[2]/p")
    public WebElement field_mensagemSucesso;

    @FindBy(xpath = "/html/body/div[2]/div/div/div/div[2]/form/div[11]/div/div/div/ul/li[8]")
    public WebElement opcao_Employeer;

    @FindBy(xpath = "/html/body/div[2]/div/div/div/div[2]/form/div[14]/div[2]/p/a[2]")
    public WebElement link_goBackToList;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[3]/input")
    public WebElement field_searchName;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/tbody/tr/td[1]/input")
    public WebElement checkBox_action;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[2]/div[1]/a")
    public WebElement button_Delete;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div/div/div[2]/p[2]")
    public WebElement text_alert;

    @FindBy(className = "delete-multiple-confirmation-button")
    public WebElement buttonDelete_popUp;

    @FindBy(css = "/html/body/div[4]/span[3]/p")
    public WebElement alert_DeleteSucess;


    public PageBootStrapTheme selecionarOpcaoNoCombo() {

        Select combo = new Select(combo_versao);

        combo.selectByIndex(1);
        return this;
    }

    public PageBootStrapTheme clickBotaoAddCustomer(){
        click(button_addCustomer);
        return this;
    }

    public void clicarAguardarPaginaPreenchimento(){
        new PageBootStrapTheme(driver).
                selecionarOpcaoNoCombo().
                clickBotaoAddCustomer();
    }

    public PageBootStrapTheme preencherCampos(){

        esperarTempoFixado(4000);

        preencherCampo(field_name,"Teste Sicredi");
        preencherCampo(field_lastName, "Teste");
        preencherCampo(field_contactFirstName,"Igor Monteiro");
        preencherCampo(field_phone, "51 9999-9999");
        preencherCampo(field_addressLine1,"Av Assis Brasil, 3970");
        preencherCampo(field_addressLine2,"Torre D");
        preencherCampo(field_city,"Porto Alegre");
        preencherCampo(field_state,"RS");
        preencherCampo(field_postalCode, "91000-000");
        preencherCampo(field_country, "Brasil");
        new PageBootStrapTheme(driver).selectBox();
        preencherCampo(field_creditLimit,"200");

        esperarTempoFixado(3000);

        return this;
    }

    public PageBootStrapTheme clickBotaoSaveEValidarMensagem(){
        esperarTempoFixado(4000);
        click(button_save);
        esperarTempoFixado(4000);
        Assert.assertEquals("Your data has been successfully stored into the database. Edit Customer or Go back to list",
        field_mensagemSucesso.getText());

        return this;
    }

    public PageBootStrapTheme selectBox() {
        esperarTempoFixado(4000);
        WebElement selectValue = opcao_Employeer;
        field_selectFromEmployer.click();
        esperarTempoFixado(4000);
        selectValue.click();
        return this;
    }

    public PageBootStrapTheme pesquisarRegistro(){

            link_goBackToList.click();
            field_searchName.sendKeys("teste");
            esperarTempoFixado(4000);

            return this;
    }

    public PageBootStrapTheme deletarRegistro(){

        checkBox_action.click();
        button_Delete.click();
        esperarTempoFixado(3000);

        Assert.assertEquals("Are you sure that you want to delete this 1 item?",text_alert.getText());
        esperarTempoFixado(3000);
        buttonDelete_popUp.click();
        buttonDelete_popUp.isSelected();
        esperarTempoFixado(4000);

        WebDriverWait wait = new WebDriverWait(driver, 7);
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.accept();
        Assert.assertTrue(alert.getText().contains("Your data has been successfully deleted from the database."));

        return this;
    }
}
